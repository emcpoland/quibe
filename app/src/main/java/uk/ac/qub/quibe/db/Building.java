package uk.ac.qub.quibe.db;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;

public class Building extends RealmObject {
    public static final String NAME = "name";
    public static final String CODE = "code";
    public static final String DESCRIPTION = "description";
    public static final String IMAGE = "image";
    public static final String LAT = "lat";
    public static final String LNG = "lng";
    public static final String HOTSPOTS = "hotspots";

    private String name;
    private String code;
    private String description;
    private String image;
    private float lat;
    private float lng;
    private RealmList<Hotspot> hotspots;

    public static Building loadByName(Realm realm, String name) {
        return realm.where(Building.class).equalTo(NAME, name).findFirst();
    }

    /**
     * Get hue color based on building name
     * Unfortunately due to restriction with RealmObject
     * All methods must be static
     */
    public static int getHueColor(Building building) {
        return Math.abs(building.getName().hashCode()) % 360;
    }

    /**
     * Getters and Setters are overwritten by the Realm proxy class
     */
    public String getName() {return null;}
    public void setName(String name) {}
    public String getCode() {return null;}
    public void setCode(String code) {}
    public String getDescription() {return null;}
    public void setDescription(String description) {}
    public String getImage() {return null;}
    public void setImage(String image) {}
    public float getLat() {return -1;}
    public void setLat(float lat) {}
    public float getLng() {return -1;}
    public void setLng(float lng) {}
    public RealmList<Hotspot> getHotspots() {return null;}
    public void setHotspots(RealmList<Hotspot> hotspots) {}
}
