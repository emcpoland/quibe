package uk.ac.qub.quibe;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.core.CrashlyticsCore;

import java.io.IOException;
import java.io.InputStream;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;
import uk.ac.qub.quibe.db.Building;
import uk.ac.qub.quibe.misc.CrashlyticsTree;

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        // Setup crash reporting
        CrashlyticsCore core = new CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build();
        Fabric.with(this, new Crashlytics.Builder().core(core).build());

        // Setup logging
        if (BuildConfig.DEBUG) Timber.plant(new Timber.DebugTree());
        else Timber.plant(new CrashlyticsTree());

        initialiseDB(this);
    }

    /**
     * Seeds the database if its empty
     */
    private static void initialiseDB(Context context) {
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder(context).deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(realmConfiguration);

        Realm realm = Realm.getDefaultInstance();
        try {
            if(realm.isEmpty()) loadJsonFromStream(context, realm);
        } catch (IOException e) {
            Timber.e(e, "Failed to initialise database");
        }
        realm.close();
    }

    /**
     * Open building.json and persists it to the DB
     * Ready to be searched etc
     */
    private static void loadJsonFromStream(Context context, Realm realm) throws IOException {
        InputStream stream = context.getAssets().open("buildings.json");

        realm.beginTransaction();
        try {
            realm.createAllFromJson(Building.class, stream);
            realm.commitTransaction();
        } catch (IOException e) {
            realm.cancelTransaction();
            throw e;
        } finally {
            if (stream != null) {
                stream.close();
            }
        }
    }

}