package uk.ac.qub.quibe.ui.fragments;

import android.Manifest;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.wifi.ScanResult;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.moonmonkeylabs.realmrecyclerview.RealmRecyclerView;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmResults;
import uk.ac.qub.quibe.R;
import uk.ac.qub.quibe.WifiLocator;
import uk.ac.qub.quibe.db.Building;
import uk.ac.qub.quibe.db.Hotspot;
import uk.ac.qub.quibe.ui.activities.DetailActivity;
import uk.ac.qub.quibe.ui.adapters.BuildingAdapter;

/**
 * Displays a list of building, which the user can click to obtain a more detailed view
 * Also shows the current QUB hotspot, which allows for a precise location to be given
 */
public class MainFragment extends Fragment implements BuildingAdapter.BuildingAdapterListener, WifiLocator.WifiLocatorInterface, SearchView.OnQueryTextListener {
    //private static final String SSID = "virginmedia6394378";
    //private static final String SSID = "WiredSSID";
    private static final String SSID = "_QUB_WiFi"; // SSID to watch for

    @Bind(R.id.list_building)
    RealmRecyclerView buildingRecyclerView;

    @Bind(R.id.collapsed_toolbar)
    Toolbar collapsedToolbar;

    @Bind(R.id.appbar)
    AppBarLayout appbar;

    private Realm realm;
    private BuildingAdapter buildingAdapter;
    private WifiLocator wifiLocator;

    private Building currentBuilding;
    private Hotspot currentHotspot;


    ////////////////////////////////////////// General Setup  //////////////////////////////////////////

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        realm = Realm.getDefaultInstance();
        buildingAdapter = new BuildingAdapter(getContext(), this);

        // Obtain current location by inspecting BSSID, we need permission first though
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
        }
        else {
            wifiLocator =  new WifiLocator(getActivity(), SSID, this);
        }

        // Set default empty query
        // We dont want anything filtered out
        onQueryTextChange("");

        // Add search action
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);

        // Could argue this belongs in Activity and not the fragment
        ((AppCompatActivity) getActivity()).setSupportActionBar(collapsedToolbar);

        // Set the adapter to display the buildings
        buildingRecyclerView.setAdapter(buildingAdapter);

        // Apply custom subtitle styling
        setupSubtitleView();
        return view;
    }

    /**
     * We dont have access to the subtitle textview within XML
     * So we need to apply these properties in code
     */
    private void setupSubtitleView() {
        TextView subtitleTextView = (TextView)collapsedToolbar.getChildAt(1);
        subtitleTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_location, 0, 0, 0);
        subtitleTextView.setCompoundDrawablePadding(10);
        subtitleTextView.setId(R.id.building_description);

        // Set up animation property if device supports it
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            subtitleTextView.setTransitionName("building_description");
        }

        ssidNotFound(); // Apples correct default title bar, in the case that location permission wasnt granted
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.activity_main, menu); // Inflate the search view
        // We want a filter as opposed to a search, so were gonna implement are own logic
        // And not use the systems framework
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // If search is clicked, collapse the actionbar, so we can view the results better
        if(item.getItemId()==R.id.search) appbar.setExpanded(false);
        return super.onOptionsItemSelected(item);
    }

    /**
     * Register for changes in Wifi hotspots, only when the activity is viewable
     */
    @Override
    public void onResume() {
        super.onResume();
        if(wifiLocator!=null) wifiLocator.register();
    }

    @Override
    public void onPause() {
        if(wifiLocator!=null) wifiLocator.unregister();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void ssidNotFound() {
        currentBuilding = null;
        currentHotspot = null;

        collapsedToolbar.setTitle(getActivity().getTitle());
        collapsedToolbar.setSubtitle(null);
    }

    @Override
    public void ssidLocated(ScanResult wifi) {
        // A QUB hotspot has been found, search the BSSID to see if its within our database
        // Unfortunately realm doesnt support reverse FK lookup, so we need to make two queries, no big deal
        currentBuilding = realm.where(Building.class).equalTo(Building.HOTSPOTS + "." + Hotspot.BSSID, wifi.BSSID).findFirst();
        currentHotspot = realm.where(Hotspot.class).equalTo(Hotspot.BSSID, wifi.BSSID).findFirst();

        if(currentHotspot != null) {
            // The hotspot it known, display the relevant information
            collapsedToolbar.setTitle(currentBuilding.getName());
            collapsedToolbar.setSubtitle(currentHotspot.getCaption());
        }
        else {
            // The hotspot isn't known, display its BSSID, so that we can manually record and add it to the database
            collapsedToolbar.setTitle(getString(R.string.unknown_locaton));
            collapsedToolbar.setSubtitle(wifi.BSSID);
        }
    }

    /**
     * OnClick listener for current building/hotspot
     */
    @OnClick(R.id.collapsed_toolbar)
    void onCurrentBuildingClicked() {
        if(currentBuilding != null) {
            // The known hotspot has been clicked, launch the details page for the relevant building
            DetailActivity.start(getActivity(), currentBuilding, getView().findViewById(R.id.appbar));
        }
        else {
            // The hotspot isn't known, we wish to make it easy to add to known locations
            // So provide the ability to copy and paste the BSSID
            ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
            clipboard.setPrimaryClip(ClipData.newPlainText("Unknown BSSID", collapsedToolbar.getSubtitle()));
        }
    }

    /**
     * OnClick listener for selected building within list
     */
    @Override
    public void onBuildingClick(Building building, View view) {
        // Open details page when a building was clicked
        DetailActivity.start(getActivity(), building, view);
    }

    /**
     * The user finally granted permisions, load the WifiLocator now
     */
    @Override
    public void onRequestPermissionsResult (int requestCode, String[] permissions, int[] grantResults) {
        if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            wifiLocator =  new WifiLocator(getActivity(), SSID, this);
            wifiLocator.register();
        }
    }

    /**
     * Users search query has changed
     */
    @Override
    public boolean onQueryTextChange(String query) {
        RealmResults<Building> queryResults;

        // If its empty, show all buidlings
        // Otherwise search the building name, description and code for matches
        if (query.isEmpty()) queryResults = realm.allObjects(Building.class);
        else queryResults = realm.where(Building.class)
                .contains(Building.NAME, query, Case.INSENSITIVE)
                .or().contains(Building.CODE, query, Case.INSENSITIVE)
                .or().contains(Building.DESCRIPTION, query, Case.INSENSITIVE).findAll();

        // Update the list of buildings
        buildingAdapter.setBuildings(queryResults);

        return true; // We handled the query ourselves, prevents the system from doing so
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true; // We handled the query ourselves, prevents the system from doing so
    }

}
