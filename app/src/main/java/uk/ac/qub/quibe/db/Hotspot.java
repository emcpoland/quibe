package uk.ac.qub.quibe.db;

import io.realm.RealmObject;

public class Hotspot extends RealmObject {
    public static final String BUILDING = "building";
    public static final String BSSID = "bssid";
    public static final String CAPTION = "caption";

    private Building building;
    private String bssid;
    private String caption;

    /**
     * Getters and Setters are overwritten by the Realm proxy class
     */
    public Building getBuilding() {return null;}
    public void setBuilding(Building building) {}
    public String getBssid() {return null;}
    public void setBssid(String bssid) {}
    public String getCaption() {return null;}
    public void setCaption(String code) {}
}
