package uk.ac.qub.quibe.misc;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.util.AttributeSet;
import android.view.View;

/**
 * Patches BottomSheetBehavior to correctly handle the case of being expanded by default
 * http://stackoverflow.com/a/36001158/1167880
 */
public class ExpandedBottomSheetBehavior<V extends View> extends android.support.design.widget.BottomSheetBehavior<V> {
    private Integer state = STATE_EXPANDED;

    public ExpandedBottomSheetBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onLayoutChild(final CoordinatorLayout parent, final V child, final int layoutDirection) {
        if (state != null) {
            SavedState dummySavedState = new SavedState(super.onSaveInstanceState(parent, child), state);
            super.onRestoreInstanceState(parent, child, dummySavedState);
            state = null;
        }
        return super.onLayoutChild(parent, child, layoutDirection);
        /**
         * Unfortunately its not good enough to just call setState(STATE_EXPANDED); after super.onLayoutChild
         * The reason is that an animation plays after calling setState. This can cause some graphical issues with other layouts
         * Instead we need to use setInternalState, however this is a private method.
         * The trick is to utilise onRestoreInstance to call setInternalState immediately and indirectly
         */
    }

    public void setStateFix(int state) {
        this.state = state;
        super.setState(state);
    }

}
