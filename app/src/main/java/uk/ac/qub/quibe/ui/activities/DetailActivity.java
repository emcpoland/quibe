package uk.ac.qub.quibe.ui.activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import jp.wasabeef.glide.transformations.BlurTransformation;
import uk.ac.qub.quibe.R;
import uk.ac.qub.quibe.db.Building;
import uk.ac.qub.quibe.misc.PaletteImageViewTarget;
import uk.ac.qub.quibe.ui.transisitions.GrowFloatingActionButton;
import uk.ac.qub.quibe.ui.transisitions.ShrinkFloatingActionButton;

public class DetailActivity extends AppCompatActivity implements PaletteImageViewTarget.PaletteTargetListener {
    private static final String EXTRA_BUILDING_ID = "EXTRA_BUILDING_ID";

    private Realm realm;
    private Building building;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbarLayout;

    @Bind(R.id.header_image)
    ImageView image;

    @Bind(R.id.building_description)
    TextView description;

    @Bind(R.id.button_directions)
    FloatingActionButton directionsButton;

    ////////////////////////////////////////// Starting helpers //////////////////////////////////////////

    public static void start(Activity activity, Building building, View view) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)  start(activity, building);
        else activity.startActivity(createIntent(activity, building), getActivityOptions(activity, view));
    }

    public static void start(Activity activity, Building building) {
        activity.startActivity(createIntent(activity, building));
    }

    private static Intent createIntent(Activity activity, Building building) {
        return new Intent(activity, DetailActivity.class).putExtra(EXTRA_BUILDING_ID, building.getName());
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        realm = Realm.getDefaultInstance();

        setupTransitions();
        setSupportActionBar(toolbar);

        building = Building.loadByName(realm, getIntent().getStringExtra(EXTRA_BUILDING_ID));

        toolbarLayout.setTitle(building.getName());
        description.setText(building.getDescription());

        // Load building image and extract colors
        Glide.with(this).load(building.getImage())
                .asBitmap().into(new PaletteImageViewTarget(image, this));

        Glide.with(this).load(building.getImage())
                .bitmapTransform(new BlurTransformation(this))
                .into((ImageView) findViewById(R.id.background));
    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    /**
     * On obtaining palette from image, color the display
     */
    @Override
    public void onPaletteGenerated(Palette palette) {
        final int defaultColor = ContextCompat.getColor(this, R.color.default_palette);
        ColorDrawable selectedColor = new ColorDrawable(palette.getMutedColor(defaultColor));

        directionsButton.setBackgroundTintList(ColorStateList.valueOf(palette.getVibrantColor(defaultColor)));
        toolbarLayout.setContentScrimColor(selectedColor.getColor());

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(selectedColor.getColor());
            getWindow().setStatusBarColor(selectedColor.getColor());
            startPostponedEnterTransition();
        }
    }

    @OnClick(R.id.button_directions)
    void onDirectionsClick(View view) {
        MapActivity.start(this, building, view);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setupTransitions() {
        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) return;
        postponeEnterTransition();
        getWindow().setSharedElementsUseOverlay(false);
        getWindow().setAllowEnterTransitionOverlap(true);
        getWindow().setEnterTransition(new GrowFloatingActionButton(directionsButton));
        getWindow().setReturnTransition(new ShrinkFloatingActionButton(directionsButton, R.id.header_image));
    }

    /**
     * Specifies which views are shared
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static Bundle getActivityOptions(Activity activity, View view) {
        return ActivityOptions.makeSceneTransitionAnimation(activity,
                new Pair<>(view.findViewById(R.id.building_header), "building_header"),
                new Pair<>(view.findViewById(R.id.building_description), "building_description")).toBundle();
    }
}
