package uk.ac.qub.quibe.ui.transisitions;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.transition.Fade;
import android.transition.Transition;
import android.view.View;

/**
 * A Transition to fade an activity and shrink a FloatingActionButton
 */
@TargetApi(Build.VERSION_CODES.KITKAT)
public class ShrinkFloatingActionButton extends Fade implements Transition.TransitionListener {
    private final FloatingActionButton floatingActionButton;
    private final Integer intermediateAnchorId;

    public ShrinkFloatingActionButton(FloatingActionButton floatingActionButton) {
        this(floatingActionButton, null);
    }

    public ShrinkFloatingActionButton(FloatingActionButton floatingActionButton, Integer intermediateAnchorId) {
        this.floatingActionButton = floatingActionButton;
        this.intermediateAnchorId = intermediateAnchorId;
        excludeTarget(floatingActionButton.getId(), true);
        addListener(this);
    }

    @Override
    public void onTransitionStart(Transition transition) {
        floatingActionButton.hide();
        /*
            An animation bug was introduced in 23.2.0, regarding the FAB
            However 23.2.1 was required inorder to have the bottom page
            The bug was caused by newly introduced code,
            in which the visibility of the FAB button was not being correctly calculated during a transition
            https://github.com/android/platform_frameworks_support/commit/fc780bab91bd4275ae2c3b75c3dfb327e008e4db#diff-5036a18b1384a8e3fb5de6b930652044

            The new logic was only executed when the FAB was anchored to an App Bar,
            The solution was to change the anchor of the FAB to the image when we transition out
            It meant the FAB wouldn't be anchored to the Appbar when the poor logic was executed
            But through anchoring it to similar view (image), it still animated back correctly
            We need to change the anchor from the appbar to the image, only when we transition
            Otherwise the animation wouldn't work when collapsing the app bar
            The support ticket has been raised
         */
        // Animation bug caused by
        if(intermediateAnchorId != null) {
            ((CoordinatorLayout.LayoutParams) floatingActionButton.getLayoutParams()).setAnchorId(intermediateAnchorId);
        }
    }

    @Override
    public void onTransitionEnd(Transition transition) {
        floatingActionButton.setVisibility(View.INVISIBLE);
        removeListener(this);
    }

    @Override
    public void onTransitionCancel(Transition transition) {

    }

    @Override
    public void onTransitionPause(Transition transition) {

    }

    @Override
    public void onTransitionResume(Transition transition) {

    }
}
