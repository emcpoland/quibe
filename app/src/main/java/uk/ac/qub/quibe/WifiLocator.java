package uk.ac.qub.quibe;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
/**
 * Listens for hotspots which match a particular SSID
 */
public class WifiLocator {
    private final Activity activity;
    private final String ssid;
    private WifiManager  wifiManager;
    private WifiLocatorInterface wifiLocatorInterface;
    private ScanResult latestResult;

    public interface WifiLocatorInterface {
        void ssidNotFound();
        void ssidLocated(ScanResult wifi);
    }

    public WifiLocator(Activity activity, String ssid, WifiLocatorInterface wifiLocatorInterface) {
        this.activity = activity;
        this.ssid = ssid;
        this.wifiLocatorInterface = wifiLocatorInterface;
        wifiManager = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
    }

    /**
     * Register to know about new hotspots as they are scanned
     * Also need to report back any current hotspots
     */
    public void register() {
        activity.registerReceiver(wifiScanReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        reportSSID();
    }

    public void unregister() {
        activity.unregisterReceiver(wifiScanReceiver);
    }

    /**
     * Listens for new scan results to report
     */
    private final BroadcastReceiver wifiScanReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            reportSSID();
        }
    };

    /**
     * Report back (Using the listener) any matching SSIDs
     */
    private void reportSSID() {
        ScanResult foundWifi = null;

        for(ScanResult wifi:wifiManager.getScanResults()) {
            if(wifi.SSID.equals(ssid)) foundWifi = wifi;
        }

        if(foundWifi == null) wifiLocatorInterface.ssidNotFound();
        else if(!foundWifi.equals(latestResult)) wifiLocatorInterface.ssidLocated(foundWifi);

        latestResult = foundWifi;
    }

}
