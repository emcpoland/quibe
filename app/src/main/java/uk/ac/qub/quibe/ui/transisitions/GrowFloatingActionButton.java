package uk.ac.qub.quibe.ui.transisitions;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.transition.Fade;
import android.transition.Transition;
import android.view.View;

/**
 * A Transition to fade an activity and grow a FloatingActionButton
 */
@TargetApi(Build.VERSION_CODES.KITKAT)
public class GrowFloatingActionButton extends Fade implements Transition.TransitionListener {
    FloatingActionButton floatingActionButton;

    public GrowFloatingActionButton(FloatingActionButton floatingActionButton) {
        this.floatingActionButton = floatingActionButton;
        excludeTarget(floatingActionButton.getId(), true);
        addListener(this);
    }

    @Override
    public void onTransitionStart(Transition transition) {
        floatingActionButton.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onTransitionEnd(Transition transition) {
        floatingActionButton.show();
        removeListener(this);
    }

    @Override
    public void onTransitionCancel(Transition transition) {

    }

    @Override
    public void onTransitionPause(Transition transition) {

    }

    @Override
    public void onTransitionResume(Transition transition) {

    }
}
