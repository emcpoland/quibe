package uk.ac.qub.quibe.ui.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.ClusterManager.OnClusterItemClickListener;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import butterknife.ButterKnife;
import io.realm.Realm;
import timber.log.Timber;
import uk.ac.qub.quibe.R;
import uk.ac.qub.quibe.db.Building;
import uk.ac.qub.quibe.ui.activities.DetailActivity;
import uk.ac.qub.quibe.ui.adapters.BuildingAdapter;

/**
 * Displays a map with all the buildings marked on it
 * The markers are clustered considering their close proximity
 * Clicking a marker displays a summary at the bottom of the screen
 * Clicking the summary launches the details page
 */
public class OverviewMapFragment extends Fragment implements OnMapReadyCallback, BuildingAdapter.BuildingAdapterListener, OnClusterItemClickListener<OverviewMapFragment.BuildingMarker> {
    private static final int CAMERA_PADDING_PX = 200;

    private Realm realm;
    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private BottomSheetDialog buildingDialog;
    private ClusterManager<BuildingMarker> buildingClusterManager;

    ////////////////////////////////////////// General Setup  //////////////////////////////////////////

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        buildingDialog = new BottomSheetDialog(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_overview_map, container, false);
        ButterKnife.bind(this, view);

        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        return view;
    }

    @Override
    public void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * On selecting a marker, a BottomSheet dialog is shown
     */
    @Override
    public boolean onClusterItemClick(BuildingMarker buildingMarker) {
        // Inflates CardView for a single building
        View view = BuildingAdapter.singleBind(buildingDialog.getLayoutInflater(), buildingMarker.getBuilding(), this);
        buildingDialog.setContentView(view);
        buildingDialog.show();
        return true;
    }

    /**
     * When the BottomSheet dialog is clicked it should launch the details page and dimsiss the dialog
     */
    @Override
    public void onBuildingClick(Building building, View view) {
        DetailActivity.start(getActivity(), building, view);
        buildingDialog.dismiss();
    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * When the overview map is obscured, we wish to show the map without markers
     */
    public void showDetail() {
        if(checkUnsupported()) return;

        buildingClusterManager.clearItems();
        for(Building building: realm.allObjects(Building.class)) {
            buildingClusterManager.addItem(new BuildingMarker(building));
        }
        buildingClusterManager.cluster();
    }

    /**
     * When the overview map if fully visible, we wish to show the detailed map with all markers on it
     */
    public void showMinimal() {
        if(checkUnsupported()) return;

        buildingClusterManager.clearItems();
        buildingClusterManager.cluster();
    }

    /**
     * Small helper to prevent unsupported operations from occuring when Google Services hasn't been installed
     */
    private boolean checkUnsupported() {
        if(map == null) {
            Timber.e("Map has yet to load, most likely due to not having Google Services installed");
            return true;
        }
        else return false;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.map = googleMap;

        // Show current location, if the permission has been granted
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
        }

        // Disable the Maps UI, we want a very basic UI for this map
        setupMapUi();

        // Provides the ability to merge markers when they are close together, to keep the UI simple
        setupClustering();

        // Minimal UI by default
        showMinimal();

        // Setup default camera angle, to include all markers
        adjustCamera();
    }

    private void setupMapUi() {
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.getUiSettings().setMapToolbarEnabled(false);
        map.getUiSettings().setCompassEnabled(false);
    }

    private void setupClustering() {
        buildingClusterManager = new ClusterManager<>(getContext(), map);
        buildingClusterManager.setOnClusterItemClickListener(this);
        buildingClusterManager.setRenderer(new BuildingMarkerRenderer(getContext(), map, buildingClusterManager));

        map.setOnCameraChangeListener(buildingClusterManager);
        map.setOnMarkerClickListener(buildingClusterManager);
    }

    private void adjustCamera() {
        // Calculates bounds, such that all markers are included
        final LatLngBounds.Builder bounds = new LatLngBounds.Builder();
        for (Building building : realm.allObjects(Building.class)) {
            LatLng position = new LatLng(building.getLat(), building.getLng());
            bounds.include(position);
        }

        // Moves the camera as soon as the map view has been full initialised
        // We need to wait until the view has been fully inflated
        // As it needs to obtain its size inorder to position the camera appropriately
        // noinspection ConstantConditions, IntelliJ your wrong
        mapFragment.getView().post(new Runnable() {
            @Override
            public void run() {
                map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds.build(), CAMERA_PADDING_PX));
            }
        });
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Inorder to show the markers merging into one, we need to use ClusterManager
     * ClusterManager requires to be passed a set of ClusterItems
     * Therefore we have created this simple inner class to hold each cluster item
     * It also gives us the added benefit of obtaining the building within the onClick listener
     */

    protected static class BuildingMarker implements ClusterItem {
        private final Building building;
        private final LatLng position;

        public BuildingMarker(Building building) {
            this.building = building;
            position = new LatLng(building.getLat(), building.getLng());
        }

        @Override
        public LatLng getPosition() {
            return position;
        }

        public Building getBuilding() {
            return building;
        }
    }

    /**
     * Provides custom styling for each marker depending on the building it represents
     */

    private static class BuildingMarkerRenderer extends DefaultClusterRenderer<BuildingMarker> {

        public BuildingMarkerRenderer(Context context, GoogleMap map, ClusterManager<BuildingMarker> clusterManager) {
            super(context, map, clusterManager);
        }

        @Override
        protected void onBeforeClusterItemRendered(BuildingMarker buildingMarker, MarkerOptions markerOptions) {
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(Building.getHueColor(buildingMarker.getBuilding())));
        }

    };

    //////////////////////////////////////////////////////////////////////////////////////////////////////

}