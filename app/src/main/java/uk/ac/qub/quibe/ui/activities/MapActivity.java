package uk.ac.qub.quibe.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import io.realm.Realm;
import timber.log.Timber;
import uk.ac.qub.quibe.R;
import uk.ac.qub.quibe.db.Building;
import uk.ac.qub.quibe.ui.transisitions.CircularActivityTransition;


public class MapActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMyLocationChangeListener, RoutingListener, CircularActivityTransition.ActivityTransitionListener {
    private static final String EXTRA_BUILDING_ID = "EXTRA_BUILDING_ID";

    private static final float DEFAULT_ZOOM_LEVEL = 17;
    private static final int ZOOM_PADDING_PX = 200;
    private static final long UPDATE_DIRECTIONS_DELAY_MS = TimeUnit.SECONDS.toMillis(10);

    private Building building;
    private LatLng currentLocation;
    private LatLng destination;
    private Route route;

    private CircularActivityTransition circularTransition;
    private GoogleMap map;
    private Realm realm;

    ////////////////////////////////////////// Starting helper //////////////////////////////////////////

    public static void start(Activity activity, Building building, View view) {
        final Intent intent = new Intent(activity, MapActivity.class)
                .putExtra(EXTRA_BUILDING_ID, building.getName())
                .putExtras(CircularActivityTransition.createBundle(view));
        activity.startActivity(intent);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);
        realm = Realm.getDefaultInstance();

        // Sets up the circular reveal transitions

        circularTransition = new CircularActivityTransition(this, this);
        circularTransition.executeEnterTransition();

        building = Building.loadByName(realm, getIntent().getStringExtra(EXTRA_BUILDING_ID));
        destination = new LatLng(building.getLat(),building.getLng());

        // Obtain map callbacks
        ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setPadding(0, 50, 0, 0); // Sets padding of map, so that MyLocation isn't under the status bar

        /**
         * Provide a default camera for now
         * When we find the users locations / route we will update the camera again to include the route boundary
         */
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(destination, DEFAULT_ZOOM_LEVEL));

        /**
         * Attempt to get current locations
         * We may be blocked due to the user not granting the required permission
         */
        try {
            map.setMyLocationEnabled(true);
            map.setOnMyLocationChangeListener(this);
        }
        catch (SecurityException e) {
            Timber.e(e, "Location permission not granted");
        }

        updateMap();
    }

    /**
     * Updates the display of the map
     * Markers, route etc
     */
    private void updateMap() {
        map.clear();
        map.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.defaultMarker(Building.getHueColor(building)))
                .position(destination)
                .title(building.getName()));

        // Plot directions if we have them
        if(route != null) {
            map.addPolyline(route.getPolyOptions())
                    .setColor(ContextCompat.getColor(this, R.color.route_color));
        }
    }

    @Override
    public void onMyLocationChange(Location location) {
        currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
        updateDirections();
    }

    ////////////////////////////////////////// Obtain directions //////////////////////////////////////////

    /**
     * Buffer calls when updating directions
     * Location changes often but directions don't need to change that often
     * Otherwise we may reach the API quota
     */
    private void updateDirections() {
        updateDirectionsHandler.removeCallbacksAndMessages(null);
        if(route == null) updateDirectionsHandler.post(updateDirectionsRunnable); // Don't buffer initial request
        else updateDirectionsHandler.postDelayed(updateDirectionsRunnable, UPDATE_DIRECTIONS_DELAY_MS);
    }

    private final Handler updateDirectionsHandler = new Handler();
    private final Runnable updateDirectionsRunnable = new Runnable() {
        @Override
        public void run() {
            Routing routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.WALKING)
                    .withListener(MapActivity.this)
                    .waypoints(currentLocation, destination)
                    .build();
            routing.execute();
        }
    };

    @Override
    public void onRoutingSuccess(ArrayList<Route> routes, int shortestRouteIndex) {
        final boolean firstRoute = (route == null);
        route = routes.get(shortestRouteIndex);

        // First time we obtained the route / current location, so update the camera again
        if(firstRoute) {
            map.moveCamera(CameraUpdateFactory.newLatLngBounds(route.getLatLgnBounds(), ZOOM_PADDING_PX));
        }

        // Update map / replot the directions
        updateMap();
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        Timber.e(e, "Failed to find route");
    }

    @Override
    public void onRoutingStart() {}

    @Override
    public void onRoutingCancelled() {}

    ////////////////////////////////////////// Transitions  //////////////////////////////////////////

    /**
     * Should be moved into a custom transition class
     * However there are several irregularities which makes the normal activity transition framework
     * Unsuitable for this particular transition
     * Such as only display the map grid during a transition as opposed to the actual map
     * This needs to be improved in the future
     *
     * See CircularActivityTransition for more info
     */

    @Override
    public void onEnterTransitionCompleted() {}

    @Override
    public void onBackPressed() {
        circularTransition.executeExitTransition();
    }

    @Override
    public void onExitTransitionCompleted() {
        super.onBackPressed();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////
}
