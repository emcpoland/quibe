package uk.ac.qub.quibe.ui.transisitions;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.ViewAnimationUtils;

import java.util.concurrent.TimeUnit;

import timber.log.Timber;

/**
 * A pseudo Transition which reveals the activity as circle originating from a view in the previous activity
 * We weren't able to use the existing Transition framework as the Map would not be loaded during which the transition occurs
 * We therefore made a pseudo transition which involves
 * Capturing the X,Y of a view before launching an activity with the co-ordinates stored within the intent
 * The X,Y values are then read in the launching activity and a circle reveal is then executed.
 * Inorder to create a return transition we need to capture the back button, play the conceal animation
 * And then call back to the activity to launch super.onBackPressed
 * While Overriding onBackPressed isn't an elegant solution it works well for the this situation
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class CircularActivityTransition {
    private static final String EXTRA_X = "CircularActivityTransition:EXTRA__X";
    private static final String EXTRA_Y = "CircularActivityTransition:EXTRA__Y";

    private final ActivityTransitionListener transitionListener;
    private final View view;
    private int x, y;
    private int r;

    public interface ActivityTransitionListener {
        void onEnterTransitionCompleted();
        void onExitTransitionCompleted();
    }

    public CircularActivityTransition(Activity activity, ActivityTransitionListener transitionListener) {
        this.transitionListener = transitionListener;

        // Obtain root view, which will be "revealed"
        this.view = activity.getWindow().getDecorView().getRootView();

        // Obtain origin of "reveal", from activity's launching intent
        this.x = activity.getIntent().getIntExtra(EXTRA_X, 0);
        this.y = activity.getIntent().getIntExtra(EXTRA_Y, 0);

        // Obtain radius when view has been laid out
        // Could have called this within executeEnterTransition
        // However maybe somebody might just want to call the exit transition
        // Also not a fan of posting to a handler within the constructor, but meh
        view.post(new Runnable() {
            @Override
            public void run() {
                final int w = Math.max(view.getWidth() - x, x);
                final int h = Math.max(view.getHeight() - y, y);
                CircularActivityTransition.this.r = (int)Math.hypot(w, h);
            }
        });
    }

    /**
     * Store X,Y of a view in a bundle to pass to the starting Activity
     */
    public static Bundle createBundle(View originView) {
        int[] pos = new int[2];
        originView.getLocationInWindow(pos);
        pos[0] += originView.getWidth()/2;
        pos[1] += originView.getHeight()/2;
        return createBundle(pos[0], pos[1]);
    }

    public static Bundle createBundle(int x, int y) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_X, x);
        bundle.putInt(EXTRA_Y, y);
        return bundle;
    }

    public void executeEnterTransition() {
        if(checkUnsupported()) return;

        view.post(new Runnable() {
            @Override
            public void run() {
                Animator animator = ViewAnimationUtils.createCircularReveal(view, x, y, 0, r);
                animator.addListener(enterListener);
                animator.start();
            }
        });
    }

    public void executeExitTransition() {
        if(checkUnsupported()) {
            transitionListener.onExitTransitionCompleted();
            return;
        }

        Animator animator = ViewAnimationUtils.createCircularReveal(view, x, y, r, 0);
        animator.addListener(exitListener);
        animator.start();
    }

    private final Animator.AnimatorListener enterListener = new Animator.AnimatorListener() {

        @Override
        public void onAnimationEnd(Animator animator) {
            view.setVisibility(View.VISIBLE);
            transitionListener.onEnterTransitionCompleted();
        }

        @Override
        public void onAnimationStart(Animator animator) {}
        @Override
        public void onAnimationCancel(Animator animator) {}
        @Override
        public void onAnimationRepeat(Animator animator) {}
    };

    private final Animator.AnimatorListener exitListener = new Animator.AnimatorListener() {

        @Override
        public void onAnimationEnd(Animator animator) {
            view.setVisibility(View.INVISIBLE); // Ensure the view is hidden after the animation
            transitionListener.onExitTransitionCompleted(); // Call back to activity to call super.onBackPressed()
        }

        @Override
        public void onAnimationStart(Animator animator) {}
        @Override
        public void onAnimationCancel(Animator animator) {}
        @Override
        public void onAnimationRepeat(Animator animator) {}
    };

    /**
     * Helper, to prevent unsupported operations form occurring on older versions of Android
     */
    private boolean checkUnsupported() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Timber.e("Unsupported operation, requries lollipop");
            return true;
        }
        else return false;
    }
}
