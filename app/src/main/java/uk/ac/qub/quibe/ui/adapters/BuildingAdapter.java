package uk.ac.qub.quibe.ui.adapters;

import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.RealmBasedRecyclerViewAdapter;
import io.realm.RealmResults;
import io.realm.RealmViewHolder;
import uk.ac.qub.quibe.R;
import uk.ac.qub.quibe.db.Building;

/**
 * Building Card Adapter seen within the main screen
 * Also provides an inflater for a single building as shown in the overview map
 * When clicking a single Building brings up a BottomSheet dialog
 */
public class BuildingAdapter extends RealmBasedRecyclerViewAdapter<Building, BuildingAdapter.BuildingViewHolder> {
    private final BuildingAdapterListener buildingAdapterListener;

    public interface BuildingAdapterListener {
        void onBuildingClick(Building building, View view);
    }

    public BuildingAdapter(Context context, BuildingAdapterListener buildingAdapterListener) {
        super(context, null, false, false);
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.buildingAdapterListener = buildingAdapterListener;
    }

    public void setBuildings(RealmResults<Building> buildings) {
        updateRealmResults(buildings);
    }

    protected static class BuildingViewHolder extends RealmViewHolder {
        private Building building;
        private BuildingAdapterListener buildingAdapterListener;

        @Bind(R.id.building_image)
        ImageView image;

        @Bind(R.id.building_toolbar)
        Toolbar toolbar;

        @Bind(R.id.building_description)
        TextView description;

        public BuildingViewHolder(View itemView, BuildingAdapterListener buildingAdapterListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.buildingAdapterListener = buildingAdapterListener;
        }

        // Temporary hack to stop stealing click events
        @OnClick(R.id.building_toolbar)
        public void onToolbarClick() {
            onBuildingClick();
        }

        @OnClick(R.id.building_card)
        public void onBuildingClick() {
            buildingAdapterListener.onBuildingClick(building, itemView);
        }

        public void bind(Building building) {
            this.building = building;

            toolbar.setTitle(building.getName());
            description.setText(building.getDescription());

            Glide.with(image.getContext()).load(building.getImage()).into(image);
        }
    }

    @Override
    public BuildingViewHolder onCreateRealmViewHolder(ViewGroup parent, int viewType) {
        return new BuildingViewHolder(inflater.inflate(R.layout.card_building, parent, false), buildingAdapterListener);
    }

    @Override
    public void onBindRealmViewHolder(BuildingViewHolder holder, int position) {
        holder.bind(realmResults.get(position));
    }

    /**
     * There may be cases in which we only wish to display a single building card
     * without creating an adapter and recycler view etc
     */
    public static View singleBind(LayoutInflater inflater, Building building, BuildingAdapterListener buildingAdapterListener) {
        View view = inflater.inflate(R.layout.card_building, null);
        BuildingViewHolder viewHolder = new BuildingViewHolder(view, buildingAdapterListener);
        viewHolder.bind(building);
        return view;
    }

}
