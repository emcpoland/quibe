package uk.ac.qub.quibe.ui.activities;

import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetBehavior.BottomSheetCallback;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.ac.qub.quibe.R;
import uk.ac.qub.quibe.misc.ExpandedBottomSheetBehavior;
import uk.ac.qub.quibe.ui.fragments.OverviewMapFragment;

/**
 * Provides a container for the main fragment and overview map fragment
 * Toggles the display of each fragment through expanding/collapsing the bottom sheet
 */
public class MainActivity extends AppCompatActivity {
    private OverviewMapFragment overviewMapFragment;
    private ExpandedBottomSheetBehavior bottomSheet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        overviewMapFragment = (OverviewMapFragment)getSupportFragmentManager().findFragmentById(R.id.overview_map_fragment);

        bottomSheet = (ExpandedBottomSheetBehavior)BottomSheetBehavior.from(findViewById(R.id.main_fragment));
        bottomSheet.setBottomSheetCallback(bottomPageListener);
    }

    /**
     * Toggle the bottom sheet, when map or button is clicked
    */
    @OnClick(R.id.buildings_button)
    public void onShowBuilding() {
        bottomSheet.setStateFix(BottomSheetBehavior.STATE_EXPANDED);
    }

    @OnClick(R.id.header_image)
    public void onShowMap() {
        bottomSheet.setStateFix(BottomSheetBehavior.STATE_COLLAPSED);
    }

    /**
     * Only load markers when fully expanded Keeps the UI clean
     */
    private final BottomSheetCallback bottomPageListener =  new BottomSheetCallback(){
        @Override
        public void onStateChanged (View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                overviewMapFragment.showDetail();
            }
            else if (newState == BottomSheetBehavior.STATE_SETTLING) {
                overviewMapFragment.showMinimal();
            }
        }

        @Override
        public void onSlide (View bottomSheet,float slideOffset){}
    };
}
