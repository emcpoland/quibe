package uk.ac.qub.quibe.misc;

import android.graphics.Bitmap;
import android.support.v7.graphics.Palette;
import android.widget.ImageView;

import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

/**
 * Helper class to generate a Palette from an image loaded using Glide
 */
public class PaletteImageViewTarget extends BitmapImageViewTarget {
    private PaletteTargetListener paletteTargetListener;

    /**
    * Create custom interface as opposed to reusing PaletteListener
    * As the name onGenerated isn't particularly descriptive
    */
    public interface PaletteTargetListener {
        void onPaletteGenerated(Palette palette);
    }

    public PaletteImageViewTarget(ImageView view, PaletteTargetListener paletteTargetListener) {
        super(view);
        this.paletteTargetListener = paletteTargetListener;
    }

    public void onResourceReady(final Bitmap bitmap, GlideAnimation anim) {
        super.onResourceReady(bitmap, anim);
        Palette.generateAsync(bitmap, new Palette.PaletteAsyncListener() {
            @Override
            public void onGenerated(Palette palette) {
                paletteTargetListener.onPaletteGenerated(palette);
            }
        });
    }
}
